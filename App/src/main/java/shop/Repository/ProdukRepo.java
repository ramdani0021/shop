package shop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import shop.Model.Produk;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ProdukRepo extends JpaRepository<Produk, Long> {
    @Query(value = "select * from produk where id = ?1", nativeQuery = true)
    Produk findDataById(Long id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE produk p set quantity =:quantity where p.id = :id",
            nativeQuery = true)
    void updateData(
            @Param("quantity") Integer quantity,
            @Param("id") Long id);
}
