package shop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shop.Model.DetailTransaksi;
import shop.Model.Member;

@Repository
public interface DetailTransaksiRepo extends JpaRepository<DetailTransaksi, Long> {

}
