package shop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import shop.Model.Member;
import shop.Model.Produk;
import shop.Model.Transaksi;

import java.util.List;

@Repository
public interface TransaksiRepo extends JpaRepository<Transaksi, Long> {
    @Query(value = "select * from transaksi where id_member = ?1", nativeQuery = true)
    List<Transaksi> findDataByMemberId(Integer id);

    @Query(value = "SELECT * FROM `transaksi` " +
            " where month(tanggal_transaksi) = ?1 ", nativeQuery = true)
    List<Transaksi> findDataMonth(Integer month);
}
