package shop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import shop.Model.Member;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface MemberRepo extends JpaRepository<Member, Long> {
    @Query(value = "select * from member where id = ?1", nativeQuery = true)
    Member findDataById(Long id);

    @Query(value = "select * from member where id not in " +
            "(SELECT id_member FROM `transaksi` WHERE tanggal_transaksi " +
            "BETWEEN :tanggal1 AND :tanggal2 and " +
            "id_member is not null)", nativeQuery = true)
    List<Member> findAllData(
            @Param("tanggal1") String tanggal1,
            @Param("tanggal2") String tanggal2);

    @Transactional
    @Modifying
    @Query(value = "UPDATE member m set status =:status where m.id = :id",
            nativeQuery = true)
    void updateData(
            @Param("status") Integer status,
            @Param("id") Long id);

}
