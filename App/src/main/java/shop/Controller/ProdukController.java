package shop.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shop.Model.Member;
import shop.Model.Produk;
import shop.Repository.MemberRepo;
import shop.Repository.ProdukRepo;

import java.net.URI;
import java.util.LinkedHashMap;

@RestController
@RequestMapping("/produk")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "API FOR PRODUK")
public class ProdukController {

    @Autowired
    ProdukRepo produkRepo;


    @GetMapping(path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("GET DATA BY ID")
    public ResponseEntity getData(@PathVariable Long id) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            Produk data = produkRepo.findDataById(id);
            if (data != null) {
                res.put("status", "berhasil");
                res.put("data", data);
                return ResponseEntity.ok().body(res);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            res.put("status", "gagal");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("SAVE DATA")
    public ResponseEntity saveData(@RequestBody Produk produk) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            produkRepo.save(produk);

            res.put("status", "berhasil");
            return ResponseEntity.created(URI.create("/produk")).body(res);
        } catch (Exception e) {
            res.put("status", "gagal");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
