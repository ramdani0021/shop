package shop.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shop.Model.*;
import shop.Repository.DetailTransaksiRepo;
import shop.Repository.MemberRepo;
import shop.Repository.ProdukRepo;
import shop.Repository.TransaksiRepo;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.net.URI;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;

@RestController
@RequestMapping("/transaksi")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "API FOR TRANSAKSI")
public class TransaksiController {

    @Autowired
    MemberRepo memberRepo;

    @Autowired
    TransaksiRepo transaksiRepo;

    @Autowired
    DetailTransaksiRepo detailTransaksiRepo;

    @Autowired
    ProdukRepo produkRepo;

    @Autowired
    ExcelGeneratorController excelGeneratorController;


    @GetMapping(path = "/idMember/{idMember}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("GET DATA BY ID")
    public ResponseEntity getData(@PathVariable Integer idMember) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            List<Transaksi> data = transaksiRepo.findDataByMemberId(idMember);
            if (data.size()>0) {
                res.put("status", "berhasil");
                res.put("data", data);
                return ResponseEntity.ok().body(res);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            res.put("status", "gagal");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("SAVE DATA")
    public ResponseEntity saveData(@RequestBody List<DTODetailTransaksi> detail, @RequestParam (required = false) Long idMember) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            Integer harga = 0;
            Integer totalHarga = 0;
            java.sql.Timestamp today = new Timestamp(System.currentTimeMillis());
            if (idMember != null) {
                Member member = memberRepo.findDataById(idMember);

                if (member != null && member.getStatus() == 1) {
                    for (DTODetailTransaksi data: detail) {
                        DetailTransaksi detailTransaksi = new DetailTransaksi();
                        Produk produk = produkRepo.findDataById(data.getIdProduk());
                        harga = produk.getHarga() * data.getQuantity() - produk.getHarga() * data.getQuantity()*20/100;
                        totalHarga += harga;
                        detailTransaksi.setIdMember(idMember);
                        detailTransaksi.setIdProduk(data.getIdProduk());
                        detailTransaksi.setQuantity(data.getQuantity());
                        detailTransaksi.setHarga(harga);

                        if (produk.getQuantity() - data.getQuantity() < 0) {
                            res.put("status", "gagal");
                            res.put("message", "Stok produk di bawah Jumlah Transaksi");
                            return ResponseEntity.badRequest().body(res);
                        }

                        detailTransaksiRepo.save(detailTransaksi);

                        produk.setQuantity(produk.getQuantity() - data.getQuantity());
                        produkRepo.updateData(produk.getQuantity(), produk.getId());
                    }
                } else {
                    for (DTODetailTransaksi data: detail) {
                        DetailTransaksi detailTransaksi = new DetailTransaksi();
                        Produk produk = produkRepo.findDataById(data.getIdProduk());
                        harga = produk.getHarga() * data.getQuantity();
                        totalHarga += harga;
                        detailTransaksi.setIdMember(idMember);
                        detailTransaksi.setIdProduk(data.getIdProduk());
                        detailTransaksi.setQuantity(data.getQuantity());
                        detailTransaksi.setHarga(harga);

                        if (produk.getQuantity() - data.getQuantity() < 0) {
                            res.put("status", "gagal");
                            res.put("message", "Stok produk di bawah Jumlah Transaksi");
                            return ResponseEntity.badRequest().body(res);
                        }

                        detailTransaksiRepo.save(detailTransaksi);

                        produk.setQuantity(produk.getQuantity() - data.getQuantity());
                        produkRepo.updateData(produk.getQuantity(), produk.getId());
                    }
                }

            } else {
                for (DTODetailTransaksi data: detail) {
                    DetailTransaksi detailTransaksi = new DetailTransaksi();
                    Produk produk = produkRepo.findDataById(data.getIdProduk());
                    harga = produk.getHarga() * data.getQuantity();
                    totalHarga += harga;
                    detailTransaksi.setIdMember(idMember);
                    detailTransaksi.setIdProduk(data.getIdProduk());
                    detailTransaksi.setQuantity(data.getQuantity());
                    detailTransaksi.setHarga(harga);

                    if (produk.getQuantity() - data.getQuantity() < 0) {
                        res.put("status", "gagal");
                        res.put("message", "Stok produk di bawah Jumlah Transaksi");
                        return ResponseEntity.badRequest().body(res);
                    }

                    detailTransaksiRepo.save(detailTransaksi);

                    produk.setQuantity(produk.getQuantity() - data.getQuantity());
                    produkRepo.updateData(produk.getQuantity(), produk.getId());
                }
            }
            Transaksi transaksi = new Transaksi();
            transaksi.setIdMember(idMember);
            transaksi.setTotalHarga(BigInteger.valueOf(totalHarga));
            transaksi.setTanggalTransaksi(today);
            transaksiRepo.save(transaksi);

            res.put("status", "berhasil");
            return ResponseEntity.created(URI.create("/Transaksi")).body(res);
        } catch (Exception e) {
            res.put("status", "gagal");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }


    @RequestMapping(path = "/Excel", method = RequestMethod.GET)
    @ApiOperation("EXPORT TO EXCEL")
    public ResponseEntity ExportExcel(@RequestParam(value = "Bulan") Integer bulan) throws Exception {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();

        try {
            ByteArrayInputStream in = excelGeneratorController.exportExcelTransaksi(bulan);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Transaksi.xlsx")
                    .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                    .body(new InputStreamResource(in));
        } catch (Exception e) {
            res.put("status", "false");
            res.put("message", e.getMessage());

            return ResponseEntity.notFound().build();
        }
    }
}
