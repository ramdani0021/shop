package shop.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import shop.Model.Member;
import shop.Model.Produk;
import shop.Repository.MemberRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@RestController
@RequestMapping("/member")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "API FOR MEMBER")
public class MemberController {

    @Autowired
    MemberRepo memberRepo;


    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("GET DATA BY ID")
    public ResponseEntity getData(@PathVariable Long id) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            Member data = memberRepo.findDataById(id);
            if (data != null) {
                res.put("status", "berhasil");
                res.put("data", data);
                return ResponseEntity.ok().body(res);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            res.put("status", "gagal");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
