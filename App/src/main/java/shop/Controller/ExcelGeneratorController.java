package shop.Controller;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import shop.Model.Member;
import shop.Model.Transaksi;
import shop.Repository.MemberRepo;
import shop.Repository.TransaksiRepo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ExcelGeneratorController {


    @Autowired
    TransaksiRepo transaksiRepo;

    @Autowired
    MemberRepo memberRepo;

    public ByteArrayInputStream exportExcelTransaksi(Integer bulan) throws Exception {
        String[] columnTransaksi = {"Member", "Total Belanja", "Tanggal Belanja"};

        Workbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try  {
            List<Transaksi> data = transaksiRepo.findDataMonth(bulan);

            CellStyle tanggaljam = workbook.createCellStyle();
            CreationHelper helperTs = workbook.getCreationHelper();
            tanggaljam.setDataFormat(helperTs.createDataFormat().getFormat("dd-MM-yyyy hh:mm:ss"));

            Sheet sheet = workbook.createSheet("transaksi");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 13);
            headerFont.setColor(IndexedColors.BLACK.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            Row headerRow = sheet.createRow(0);

            for (int i = 0; i < columnTransaksi.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columnTransaksi[i]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIdx = 1;
            for (Transaksi transaksi : data) {
                Member member =  new Member();
                if (transaksi.getIdMember() != null) {
                    member = memberRepo.findDataById(transaksi.getIdMember());
                }
                Row row = sheet.createRow(rowIdx);
                row.createCell(0).setCellValue(member.getNama());
                row.createCell(1).setCellValue(String.valueOf(transaksi.getTotalHarga()));
                Cell tanggalTransaksi = row.createCell(2);
                tanggalTransaksi.setCellValue(transaksi.getTanggalTransaksi());
                tanggalTransaksi.setCellStyle(tanggaljam);
                rowIdx++;
            }
            workbook.write(out);
            workbook.close();
            return new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            return null;
        }
    }
}
