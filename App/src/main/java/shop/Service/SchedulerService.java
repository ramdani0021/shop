package shop.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import shop.Model.Member;
import shop.Repository.MemberRepo;

@Service
public class SchedulerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerService.class);

    @Autowired
    MemberRepo memberRepo;

    @Scheduled(cron = "0 0 0 * * *")
    public void updateStatus() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date hariIni = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(hariIni);
        String strDate1 = formatter.format(cal.getTime());
        String tanggal1 = strDate1.concat(" ").concat("00:00:00");


        cal.add(Calendar.DATE, -30);
        String strDate = formatter.format(cal.getTime());

        String tanggal2 = strDate.concat(" ").concat("23:59:59");

        List<Member> listMember = memberRepo.findAllData(tanggal1, tanggal2);

        for (Member mem : listMember) {
            mem.setStatus(0);
            memberRepo.updateData(mem.getStatus(), mem.getId());
        }
   }
}
