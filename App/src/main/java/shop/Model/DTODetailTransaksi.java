package shop.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DTODetailTransaksi {

    @Column(name = "id_produk")
    private Long idProduk;

    @Column(name = "quantity")
    private Integer quantity;

}
